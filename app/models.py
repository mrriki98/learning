# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AbstractUser, User
from django.db import models


# Create your models here.

class MainSlider(models.Model):
    image = models.ImageField(upload_to='media/img/', verbose_name='Main Slider')
    title = models.CharField(max_length=255, verbose_name='Slider Title')

    def __str__(self):
        return self.image.name


class MainHeader(models.Model):
    image = models.ImageField(upload_to='media/img/', verbose_name='Header icon')
    title = models.CharField(max_length=255, verbose_name='Title')
    body = models.TextField(verbose_name='Header Body')

    def __str__(self):
        return str(self.title)


class Subject(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название курса')

    def __str__(self):
        return str(self.name)


class Category(models.Model):
    subject = models.ForeignKey(Subject, related_name='category')
    name = models.CharField(max_length=255, verbose_name='Название категории')

    def __str__(self):
        return str(self.name)


class Theme(models.Model):
    name = models.CharField(max_length=255, verbose_name='Название темы')
    category = models.ForeignKey(Category, related_name='theme')

    def __str__(self):
        return str(self.name)

    def get_some_data(self, user):
        some_data = ListOfData.objects.filter(
            shown=False,
            user__username=user,
        )
        if len(some_data) == 0:
            some_data = None
            return some_data
        else:
            return some_data.first()


class Data(models.Model):
    name = models.CharField(max_length=255, verbose_name='Данные')
    audio = models.FileField(verbose_name='Аудио файл', upload_to='media/audio/')
    theme = models.ForeignKey(Theme,related_name='data')

    def __str__(self):
        return str(self.name)


class UserProfile(models.Model):
    phone = models.CharField(max_length=255, verbose_name='Phone')
    user = models.OneToOneField(User, related_name='phone')

    def __str__(self):
        return str(self.phone + ' ' + self.user.username)


class ListOfData(models.Model):
    data = models.ForeignKey(Data, related_name='list')
    shown = models.BooleanField(default=False, verbose_name='Показан')
    user = models.ForeignKey(User,related_name='list_of_data')


class FAQ(models.Model):
    question = models.TextField(verbose_name='Вопрос')
    answer = models.TextField(verbose_name='Ответ')

    def __str__(self):
        return str(self.question)