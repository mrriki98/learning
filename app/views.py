# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import (
    login as auth_login,
    authenticate)
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.views import logout, login
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.base import View

from app.models import MainSlider, MainHeader, Subject, Category, Theme, UserProfile, Data, ListOfData, FAQ


# @api_view(['GET'])
# def api_root(request, format=None):
#     """
#     The entry endpoint of our API.
#     """
#     return Response({
#         'users': reverse('user-list', request=request),
#         'groups': reverse('group-list', request=request),
#     })
#
#
# class UserList(generics.ListCreateAPIView):
#     """
#     API endpoint that represents a list of users.
#     """
#     queryset = User.objects.all()
#     model = User
#     serializer_class = UserSerializer
#
#
# class UserDetail(generics.RetrieveUpdateDestroyAPIView):
#     """
#     API endpoint that represents a single user.
#     """
#     model = User
#     serializer_class = UserSerializer
#     def get_queryset(self):
#         query_set = User.objects.filter(pk=self.kwargs['pk'])
#         return query_set
from learning.forms import UserAddForm


class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data()
        context['sliders'] = MainSlider.objects.all()
        context['headers'] = MainHeader.objects.all()
        return context


class CategoriesView(TemplateView):
    template_name = 'category.html'

    def dispatch(self, request, *args, **kwargs):
        return redirect(reverse('category', kwargs={'subject_id': 1}))


class CategoryView(TemplateView):
    template_name = 'categories.html'

    def get_context_data(self, **kwargs):
        context = super(CategoryView, self).get_context_data()
        context['subject'] = Subject.objects.all()
        context['categories'] = Category.objects.filter(subject_id=kwargs['subject_id'])
        return context


class ThemesView(TemplateView):
    template_name = 'theme.html'

    def get_context_data(self, **kwargs):
        context = super(ThemesView, self).get_context_data()
        context['subjects'] = Subject.objects.all()
        context['category'] = Category.objects.get(id=kwargs['category_id'])
        context['themes'] = Theme.objects.filter(category_id=kwargs['category_id'])
        return context


# class Login(View, LoginView):

def registration(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        phone = request.POST.get('phone')
        password = request.POST.get('password')
        if username and phone and password:
            User.objects.create(
                username=username,
                password=password,
                is_active=True,
                is_staff=True,
                is_superuser=True
            )
            user = User.objects.get(username=username)
            UserProfile.objects.create(
                user=user,
                phone=phone
            )
            return redirect(reverse('categories'))
    return redirect(reverse('index'))


def auth_logout(request):
    logout(request)
    return redirect(reverse("index"))


def redirect_home(request):
    return redirect(reverse("index"))


class ProfileView(TemplateView):
    template_name = 'profile.html'

    def get_context_data(self, **kwargs):
        context= super(ProfileView, self).get_context_data()
        context['subjects'] = Subject.objects.all()
        return context

class PreLessonView(TemplateView):
    template_name = 'pre-lesson.html'

    def dispatch(self, request, *args, **kwargs):
        theme = Theme.objects.get(id=kwargs['theme_id'])
        subjects = Subject.objects.all()
        context = {
            'theme': theme,
            'subjects': subjects
        }
        return render(request, self.template_name, context)


class PreLesson(View):
    def dispatch(self, request, *args, **kwargs):
        list = Data.objects.filter(theme_id=kwargs['theme_id'])
        if len(list) != 0:
            for data in list:
                ListOfData.objects.create(
                    data_id=data.id,
                    user=request.user
                )
        return redirect(reverse('lesson', kwargs={'theme_id': kwargs['theme_id']}))


class LessonView(TemplateView):
    template_name = 'lesson.html'

    def dispatch(self, request, *args, **kwargs):
        context = {}
        context['subjects'] = Subject.objects.all()
        theme = Theme.objects.get(id=kwargs['theme_id'])
        if theme.get_some_data(request.user.get_username()) == None:
            return redirect(reverse('finish_lesson', kwargs={'theme_id': theme.id}))
        context['theme_data'] = theme.get_some_data(request.user.get_username())
        context['theme'] = theme
        return render(request, self.template_name, context)


class NextData(View):
    def dispatch(self, request, *args, **kwargs):
        list_data = ListOfData.objects.get(id=kwargs['list_id'])
        list_data.shown = True
        list_data.save()
        return redirect(reverse('lesson', kwargs={'theme_id': kwargs['theme_id']}))


class LessonFinishView(TemplateView):
    template_name = 'finish.html'

    def get_context_data(self, **kwargs):
        context = super(LessonFinishView, self).get_context_data()
        context['subjects'] = Subject.objects.all()
        context['theme'] = Theme.objects.get(id=kwargs['theme_id'])
        return context


class FAQView(TemplateView):
    template_name = 'faq.html'

    def get_context_data(self, **kwargs):
        context = super(FAQView, self).get_context_data()
        context['faqs'] = FAQ.objects.all().order_by('id')
        return context


def my_login(request):
    if request.method == 'POST':
        # username = request.POST['username']
        # password = request.POST['password']
        auth_login(request, AuthenticationForm(request).get_user())
        #
        # user = authenticate(request, username=username, password=password)
        # if user is not None:
        #     login(request, user)
        #     return redirect(reverse('profile'))
        # else:
        #     return redirect(reverse('my_login'))
    return render(request, 'registration/login.html')

class Registration(TemplateView):
    template_name = 'register.html'
    def dispatch(self, request, *args, **kwargs):
        if request.method == 'POST':
            form = UserAddForm(request.POST)
            if form.is_valid():
                form.save()
                username = form.cleaned_data.get('username')
                raw_password = form.cleaned_data.get('password1')
                user = authenticate(username=username, password=raw_password)
                login(request, user)
                return redirect(reverse('profile'))
        context={
            'form':UserAddForm()
        }
        return render(request,self.template_name,context)