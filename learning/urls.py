"""learning URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login
from django.views.generic import TemplateView

from app.views import HomeView, CategoriesView, CategoryView, ThemesView,  redirect_home, auth_logout, \
    PreLessonView, PreLesson, LessonView, LessonFinishView, NextData, ProfileView, FAQView, my_login, Registration
from learning import settings


urlpatterns = [
    url(r'^$', HomeView.as_view(), name='index'),
    url(r'^categories$', CategoriesView.as_view(), name='categories'),
    url(r'^category/(?P<subject_id>\d+)/$', CategoryView.as_view(), name='category'),
    url(r'^lesson$', TemplateView.as_view(template_name='lesson.html'), name='lesson'),
    url(r'^themes/(?P<category_id>\d+)/$', ThemesView.as_view(), name='themes'),
    url(r'^pre-lesson/(?P<theme_id>\d+)/$', login_required(PreLessonView.as_view()), name='pre-lesson'),
    url(r'^pre-lesson/(?P<theme_id>\d+)/runner$', login_required(PreLesson.as_view()), name='pre-lesson-runner'),
    url(r'^lesson/(?P<theme_id>\d+)/$', login_required(LessonView.as_view()), name='lesson'),
    url(r'^lesson/(?P<theme_id>\d+)/finish$', login_required(LessonFinishView.as_view()), name='finish_lesson'),
    url(r'^lesson/(?P<theme_id>\d+)/(?P<list_id>\d+)$', login_required(NextData.as_view()), name='next_data'),
    url(r'^admin/', admin.site.urls),

    url(r'^profile/$', login_required(ProfileView.as_view()), name='profile'),
    url(r'^accounts/profile/', redirect_home),
    url(r'^accounts/registration/$', Registration.as_view(), name='registration'),
    url(r'^accounts/login/$', login, name="my_login"),
    url(r'^accounts/logout/$', auth_logout, name="logout"),
    url(r'^FAQ/$', FAQView.as_view(), name='faq'),

]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)
